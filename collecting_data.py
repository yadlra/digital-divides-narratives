import tweepy
from tweepy.auth import OAuthHandler
import configparser
import pandas as pd
import time

start_time = time.time()


#config keys
config = configparser.ConfigParser()
config.read('config.ini')

api_key = config['twitter']['api_key']
api_key_secret = config['twitter']['api_key_secret']

access_token = config['twitter']['access_token']
access_token_secret = config['twitter']['access_token_secret']


#authenticate
auth = tweepy.OAuthHandler(api_key, api_key_secret)
auth.set_access_token(access_token, access_token_secret)

api = tweepy.API(auth)

#user tweets

user = "-------"
limit = 1001

tweets = tweepy.Cursor(api.user_timeline, screen_name = user, count = 5000, tweet_mode = 'extended').items(limit)

columns = ['User', 'Following','Followers', 'Total Tweets','Tweet', 'Retweets', 'Likes']
data = []

for tweet in tweets:
          data.append([tweet.user.screen_name,
                       tweet.user.friends_count,
                       tweet.user.followers_count,
                       tweet.user.statuses_count,
                       tweet.full_text,
                       tweet.retweet_count,
                       tweet.favorite_count,
                       ])

df = pd.DataFrame(data, columns=columns)

df.to_csv('-----.csv', encoding='utf-8')

print(df)
print("--- %s seconds ---" % (time.time() - start_time))